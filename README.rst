##########
NDD Test4P
##########

|pipeline| |coverage|

.. |pipeline| image:: https://gitlab.com/ddidier/python-ndd-test4p/badges/master/pipeline.svg
    :target: https://gitlab.com/ddidier/python-ndd-test4p/commits/master
    :alt: Pipeline Status

.. |coverage| image:: https://gitlab.com/ddidier/python-ndd-test4p/badges/master/coverage.svg
    :target: https://gitlab.com/ddidier/python-ndd-test4p/commits/master
    :alt: Coverage Report


Utilities for testing Python code.

Documentation is available at https://ddidier.gitlab.io/python-ndd-test4p/.
